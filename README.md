[![License](http://img.shields.io/:license-apache-blue.svg?style=flat)](https://bitbucket.org/newjavaonemahesh/siteprogrammer/LICENSE)

# Siteprogrammer

[![N|Solid](https://projects.spring.io/spring-boot/img/spring-logo.png)](https://projects.spring.io/spring-boot/)

The aim is to build a tiny application that reads the top 1 million sites available as a csv file, look up technology stacks used to implement each AND Finally store them to local DB.

  - One possibility is to use wappalyzer
  - Else write a web scraping program otherwise

# New Features!

  - None

> The overriding design goal I have is to
> make it happen, agnostic of the programming
> language.  Spring Batch can do some decent batch work.  No
> love for a specific programming language, if it fails to
> do the job.  But I chose Java for now.

### Tech

Siteprogrammer uses a number of open source projects to work properly:

* [Java] - version 1.8, as we all know the mostly widely used one!
* [Spring Boot] - An expert's choice for standalone production read Java apps
* [Spring Batch] - Create lightweight Spring based batch apps. v.3.0.5.  Written in JavaConfig, no XML spaghetti!!!
* [Maven] - build tool, v3.0
* [MySQL] - Most popular open source database server, also Oracle's
* [Twitter Bootstrap] - No, not really, still not there.
* [node.js] - REST API wrapper.
* [Express] - fast node.js network app framework [@tjholowaychuk]
* [Intellij] - IDE

And of course Siteprogrammer itself is open source with a [public repository][newjavaonemahesh]
 on GitHub.

### Installation
## Build with Maven

###### Prerequisites:

* Siteprogrammer requires [Java](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)~=1.8.
* Maven 3


Build and run as follows:

```sh
$ git clone git@bitbucket.org:newjavaonemahesh/siteprogrammer.git Siteprogrammer
$ cd Siteprogrammer
$ mvn clean install
$ java -jar Siteprogrammer-0.0.1-SNAPSHOT.jar target/pathToFile=C:/csv/top-1million-sites.csv
```

Spring Boot offers a fast way to build applications. It looks at your classpath and at beans you have configured, makes reasonable assumptions about what you’re missing, and adds it. With Spring Boot you can focus more on business features and less on infrastructure.
For more details visit the Spring Boot [docs](https://spring.io/docs)

### Development

Want to contribute? Great!

### Todos

 - Look up technologies stack.
 - Load them using JSON to POJO converter into DB.

License
----

[![License](http://img.shields.io/:license-apache-blue.svg?style=flat)](https://bitbucket.org/newjavaonemahesh/siteprogrammer/LICENSE)


**Free Software, Hell Yeah!**

   [newjavaonemahesh]: <https://bitbucket.org/newjavaonemahesh>
   [git-repo-url]: <https://bitbucket.org/newjavaonemahesh/siteprogrammer>
   [Java]: http://www.oracle.com/technetwork/java/index.html
   [Spring Boot]: http://projects.spring.io/spring-boot/
   [Spring Batch]: http://projects.spring.io/spring-batch/
   [Maven]: https://maven.apache.org/
   [MySQL]: https://www.mysql.com/
   [node.js]: <http://nodejs.org>
   [Twitter Bootstrap]: <http://twitter.github.com/bootstrap/>
   [express]: <http://expressjs.com>
   [Intellij]: <https://www.jetbrains.com/idea/>


