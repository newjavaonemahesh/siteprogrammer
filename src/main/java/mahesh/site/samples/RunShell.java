package mahesh.site.samples;

import org.apache.log4j.Logger;

import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class RunShell {

    private static final Logger LOG = Logger.getLogger(RunShell.class);

    private static final String BASH_CMD = "bash";

    private static final String PROG = "/home/mahesh1/workspace/Siteprogrammer/src/main/resources/hello.sh";

    private static final String[] CMD_ARRAY = {BASH_CMD, PROG};

    public static void setUpStreamGobbler(final InputStream is, StringBuffer buff) {


        new Thread(new Runnable() {
            public void run() {
                String line = null;

                InputStreamReader streamReader = new InputStreamReader(is);

                BufferedReader br = new BufferedReader(streamReader);

                try {
                    while ((line = br.readLine()) != null) {
                        buff.append(line);
                        buff.append(System.getProperty("line.separator"));
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    try {
                        br.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();


    }

    public static String callWappalyzer(String param1, StringBuffer buff1, StringBuffer buff2) {
        Process process = null;
        String result1 = null;
        String result2 = null;
        try {

            String str;

            ProcessBuilder processBuilder = new ProcessBuilder(CMD_ARRAY);
            processBuilder.environment().put("param1", param1);
            process = processBuilder.start();
            InputStream inputStream = process.getInputStream();

            setUpStreamGobbler(inputStream, buff1);

            InputStream errorStream = process.getErrorStream();

            setUpStreamGobbler(errorStream, buff2);

            //System.out.println("output:" + result1);
            //System.out.println("error: " + result2);


            //System.out.println("never returns");
            process.waitFor();

            result1 = buff1.toString();
            result2 = buff2.toString();
            LOG.debug("error: " + result2);


        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        return result1;
    }


    public static void main(String[] args) {

        StringBuffer buff1 = null;
        StringBuffer buff2 = null;

        List<String> urls = new ArrayList<>();
        //urls.add("fzi.de");
        //urls.add("birthdaycard-idea.com");
        //urls.add("madteam.net");
        //urls.add("4,nakuri.com");
        //urls.add("dracco.co.uk");
        //urls.add("imaanhammamnews.com");
        //urls.add("bluespringsfordparts.com");
        //urls.add("oitvplanos.com.br");
        //urls.add("h-online.com");
        //urls.add("x-zona.su");
        //urls.add("jsptpd.com");
        //urls.add("themodernpantry.co.uk");
        //urls.add("healthme.com.cn");
        //urls.add("egetforum.se");
        //urls.add("taxi-forum.ru");
        //urls.add("timex.com.mx");


        urls.add("http://www.youtube.com/");
        urls.add("https://facebook.com/");
        urls.add("http://baidu.com");
        urls.add("https://wikipedia.org/");
        urls.add("https://www.yahoo.com/");
        urls.add("http://www.google.co.in/");
        urls.add("http://www.qq.com/");


        urls.add("http://www.taobao.com/");
        urls.add("https://www.tmall.com/");
        urls.add("https://amazon.com/");
        urls.add("http://www.sohu.com/");
        urls.add("https://www.reddit.com/");
        urls.add("http://live.com");
        urls.add("https://twitter.com/");
        urls.add("https://www.instagram.com/");
        urls.add("http://www.sina.com.cn/");
        urls.add("https://www.360.cn");
        urls.add("http://www.jd.com/");
        urls.add("https://www.linkedin.com/");
        urls.add("http://www.google.fr/");


        for (Iterator<String> iter = urls.iterator(); iter.hasNext(); ) {
            buff1 = new StringBuffer();
            buff2 = new StringBuffer();
            String s = RunShell.callWappalyzer(iter.next(), buff1, buff2);
            System.out.println("result1: " + buff1.toString());
        }




    }


}