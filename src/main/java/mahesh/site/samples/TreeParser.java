/*
 *
 *  * Copyright [2017] [Mahesh Chandran K R]
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package mahesh.site.samples;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import mahesh.site.model.App;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

public class TreeParser {

    public static Set<App> parseTree(){
        ObjectMapper mapper = null;
        JsonNode root = null;
        Map<?,?> appMap = null;

        JsonNode apps = null;
        Iterator<JsonNode> elements = null;
        mapper = new ObjectMapper();
        Set<App> set = new LinkedHashSet<App>();

        try {
            root = mapper.readTree(new File("/home/mahesh1/workspace/Siteprogrammer/src/main/resources/json/apps.json"));
            apps = root.get("apps");
            Iterator<Map.Entry<String, JsonNode>> iterator = apps.fields();

            while (iterator.hasNext()) {
                Map.Entry<String, JsonNode> entry = iterator.next();
                String appName = entry.getKey();
                JsonNode value = entry.getValue();
                System.out.println(appName);
                App application = new App();
                application.setAppName(appName);
                set.add(application);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return set;
    }

    public static void insertToDB(Set<App> set){

        String insertAppStmt = "INSERT INTO apps"
                + "(app_name) VALUES"
                + "(?)";
        PreparedStatement pStmt = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection con= DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/millionsitesdb","root","root");

            pStmt = con.prepareStatement(insertAppStmt);


            for (App app: set) {
                pStmt.setString(1, app.getAppName());
                pStmt .executeUpdate();
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String args[]) {
        Set<App> set = TreeParser.parseTree();
        insertToDB(set);
    }
}
