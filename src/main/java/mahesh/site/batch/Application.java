/*
 * Copyright [2017] [Mahesh Chandran K R]
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package mahesh.site.batch;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class Application {

    private static final Logger LOG = Logger.getLogger(Application.class);

    public static void main(String[] args) throws Exception {
        SpringApplication.run(Application.class, args);
    }

    @Autowired
    private ObjectMapper objectMapper;

    @Bean(name="restTemplate")
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        RestTemplate restTemplate = new RestTemplate();
        //List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
        //MappingJackson2HttpMessageConverter jsonMessageConverter = new MappingJackson2HttpMessageConverter();
        //jsonMessageConverter.setObjectMapper(objectMapper);
        //messageConverters.add(jsonMessageConverter);
        //restTemplate.setMessageConverters(messageConverters);
        return restTemplate;
    }

/*    @Bean(name="dockerConfig")
    public DockerClientConfig dockerClientConfig(){
        DockerClientConfig config = DefaultDockerClientConfig.createDefaultConfigBuilder()
                .withDockerHost("xxx")
                .withDockerTlsVerify(aaa)
                .withDockerCertPath("xxx")
                .withDockerConfig("xxx")
                .withApiVersion("xxx")
                .build();

        return config;
    }

    @Bean(name="dockerCmdExecFactory")
    public DockerCmdExecFactory dockerCmdExecFactory(){
        return new NettyDockerCmdExecFactory();
    }

    @Bean(name="dockerClient")
    public DockerClient dockerClient(@Qualifier("dockerConfig") DockerClientConfig config,
                                     @Qualifier("dockerCmdExecFactory") DockerCmdExecFactory dockerCmdExecFactory){
        DockerClient docker = DockerClientBuilder.getInstance(config)
                .withDockerCmdExecFactory(dockerCmdExecFactory)
                .build();
        return docker;
    }*/

}
