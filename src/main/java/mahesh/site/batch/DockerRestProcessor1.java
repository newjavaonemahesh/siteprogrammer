/*
 *
 *  * Copyright [2017] [Mahesh Chandran K R]
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package mahesh.site.batch;

import com.fasterxml.jackson.databind.ObjectMapper;
import mahesh.site.model.Site;
import mahesh.site.samples.RunShell;
import org.apache.log4j.Logger;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

public class DockerRestProcessor1 implements ItemProcessor<Site, Site> {

    private static final Logger LOG = Logger.getLogger(DockerRestProcessor1.class);

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private ObjectMapper mapper;

    private RunShell runShellCmd;

    @Override
    public Site process(Site site) throws Exception {

        String url = site.getUrl();
        Long fileLineNo = site.getFileLineNo();
        Site newSite = retryWithWappalyzerURL(url, fileLineNo);

        boolean s = lookupStatus(newSite);

        newSite.setFileLineNo(site.getFileLineNo());
        newSite.setId(site.getId());

        //LOG.debug("leaving process method");
        //LOG.debug("sending " + newSite.getUrl() + " to writer");

        if (newSite.getApps()!=null && newSite.getApps().size()>0) {
            return newSite;
        }

        return null;


    }


    public Site retryWithWappalyzerURL(String url, Long fileLineNo) throws Exception {
        int retries = 3;
        //System.out.println("Attempting url: " + site.getUrl());
        //LOG.debug("Attempting url: " + url);


        Site newSite = null;
        StringBuffer buff1 = null;
        StringBuffer buff2 = null;
        String jsonInString = null;
        boolean retry = true;

        int i=0;


        //System.out.println("Detecting empty apps");
        //LOG.debug("Detecting empty apps");

        while(i<retries && retry) {
            buff1 = new StringBuffer();
            buff2 = new StringBuffer();
            jsonInString = this.runShellCmd.callWappalyzer(url, buff1, buff2);

            newSite = mapper.readValue(jsonInString, Site.class);
            if (newSite.getApps().size()==0){
                //System.out.println(url + " null Apps again, used " + url + " instead - Attempt-" + (i+1));
                //LOG.debug(url + " null Apps again, used " + url + " instead - Attempt-" + (i+1));
            } else if (newSite.getApps()!=null && newSite.getApps().size()==0){
                //System.out.println(" empty Apps again, used " + url + " instead - Attempt-" + (i+1));
                //LOG.debug(" empty Apps again, used " + url + " instead - Attempt-" + (i+1));
            } else if (newSite.getApps()!=null && newSite.getApps().size()>0) {
                LOG.debug(" got apps for " + url + " during - Attempt-" + (i+1));
                retry = false;
            }
            i++;
            buff1 = null;
            buff2 = null;
        }

        //System.out.println("got: " + site.getFileLineNo() + " : " + jsonInString);
        //LOG.debug("got: " + fileLineNo + " : " + jsonInString);
        newSite.setWappalyzerJson(jsonInString);
        return newSite;
    }


    public boolean lookupStatus(Site newSite){
        if (newSite.getApps().size()==0 || (newSite.getApps()!=null && newSite.getApps().size()==0)) {
            //System.out.println("We are not lucky.  No apps for " + newSite.getUrl());
            //LOG.debug("We are not lucky.  No apps for " + newSite.getUrl());
        }

        if (newSite.getApps()!=null&&newSite.getApps().size()>0) {
            //System.out.println("Has " + newSite.getApps().size() + " apps");
            LOG.debug("Has " + newSite.getApps().size() + " apps");
            return true;
        }

        return false;
    }

    public void setRunShell(RunShell runShellCmd){
        this.runShellCmd = runShellCmd;
    }
}
