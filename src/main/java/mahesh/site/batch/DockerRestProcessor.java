/*
 * Copyright [2017] [Mahesh Chandran K R]
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package mahesh.site.batch;


import com.fasterxml.jackson.databind.ObjectMapper;
import mahesh.site.model.Site;
import mahesh.site.samples.RunShell;
import org.apache.log4j.Logger;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.Map;

public class DockerRestProcessor implements ItemProcessor<Site, Site> {

    private static final Logger LOG = Logger.getLogger(DockerRestProcessor.class);

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private ObjectMapper mapper;

    private RunShell runShellCmd;

    @Override
    public Site process(Site site) throws Exception {

        String baseUrl = site.getOriginalUrl();
        Long fileLineNo = site.getFileLineNo();
        Site newSite = retryAttempt(baseUrl, fileLineNo);

        boolean s = lookupStatus(newSite);

        newSite.setFileLineNo(site.getFileLineNo());

        LOG.debug("leaving process method");
        LOG.debug("sending " + newSite.getOriginalUrl() + " to writer");

        return newSite;
    }


    public Site retryAttempt(String url, Long fileLineNo) throws Exception {

        Map<String, Object> result = new java.util.HashMap<String, Object>();

        // append http
        String httpUrl = "http://" + url;
        Site newSite = retryForNullApps(httpUrl, fileLineNo);

        if (newSite.getApps()==null) {
            //System.out.println("if it is still null, return for now");
            LOG.debug("if it is still null, return for now");
            return newSite;
        }

        if (newSite.getApps()!=null && newSite.getApps().size()==0){
            newSite = retryForEmptyApps(httpUrl, fileLineNo);
            if (newSite.getApps()!=null && newSite.getApps().size()==0) {
                //System.out.println("Given URL: " + site.getOriginalUrl());
                LOG.debug("Given URL: " + httpUrl);
                //System.out.println("Wappalyzer has another: " + newSite.getUrl());
                LOG.debug("Wappalyzer has another: " + newSite.getUrl());
                String wappUrl = newSite.getUrl();
                newSite = retryWithWappalyzerURL(wappUrl, fileLineNo);
            }
        }

        return newSite;
    }


    public Site retryForNullApps(String url, Long fileLineNo) throws Exception {
        int retries = 2;
        //System.out.println("Attempting url: " + site.getOriginalUrl());
        LOG.debug("Attempting url: " + url);

        boolean retry = true;

        int i=0;

        Site newSite = null;
        StringBuffer buff1 = new StringBuffer();
        StringBuffer buff2 = new StringBuffer();

        String jsonInString = null;


        //System.out.println("Detecting null apps");
        LOG.debug("Detecting null apps");
        while(i<retries && retry) {
            jsonInString = this.runShellCmd.callWappalyzer(url, buff1, buff2);

            newSite = mapper.readValue(jsonInString, Site.class);
            if (newSite.getApps()==null) {
                //System.out.println(url + " null Apps - Attempt-" + (i+1));
                LOG.debug(url + " null Apps - Attempt-" + (i+1));
            } else {
                //System.out.println(url + " not null Apps, but still can be empty with no data");
                LOG.debug(url + " not null Apps, but still can be empty with no data");
                retry = false;
            }
            i++;
        }

        //System.out.println("got: " + site.getFileLineNo() + " : " + jsonInString);
        LOG.debug("got: " + fileLineNo + " : " + jsonInString);
        return newSite;

    }

    public Site retryForEmptyApps(String url, Long fileLineNo) throws Exception {
        int retries = 3;
        //System.out.println("Attempting url: " + site.getOriginalUrl());
        LOG.debug("Attempting url: " + url);

        boolean retry = true;

        int i=0;

        Site newSite = null;
        StringBuffer buff1 = new StringBuffer();
        StringBuffer buff2 = new StringBuffer();
        String jsonInString = null;

        //System.out.println("Detecting empty apps");
        LOG.debug("Detecting empty apps");

        while(i<retries && retry) {
            jsonInString = this.runShellCmd.callWappalyzer(url, buff1, buff2);

            newSite = mapper.readValue(jsonInString, Site.class);
            if (newSite.getApps()!=null && newSite.getApps().size()==0){
                //System.out.println(url + " empty Apps - Attempt-" + (i+1));
                LOG.debug(url + " empty Apps - Attempt-" + (i+1));
            } else {
                //System.out.println(url + " not empty Apps, but still can be null in the first place. " +
                        //"or, should have an alternate url to lookup");
                LOG.debug(url + " not empty Apps, but still can be null in the first place. " +
                        "or, should have an alternate url to lookup");

                retry = false;
            }
            i++;
        }

        //System.out.println("got: " + site.getFileLineNo() + " : " + jsonInString);
        LOG.debug("got: " + fileLineNo + " : " + jsonInString);
        return newSite;
    }

    public Site retryWithWappalyzerURL(String url, Long fileLineNo) throws Exception {
        int retries = 2;
        //System.out.println("Attempting url: " + site.getUrl());
        LOG.debug("Attempting url: " + url);


        Site newSite = null;
        StringBuffer buff1 = new StringBuffer();
        StringBuffer buff2 = new StringBuffer();
        String jsonInString = null;
        boolean retry = true;

        int i=0;


        //System.out.println("Detecting empty apps");
        LOG.debug("Detecting empty apps");

        while(i<retries && retry) {
            jsonInString = this.runShellCmd.callWappalyzer(url, buff1, buff2);

            newSite = mapper.readValue(jsonInString, Site.class);
            if (newSite.getApps().size()==0){
                //System.out.println(url + " null Apps again, used " + url + " instead - Attempt-" + (i+1));
                LOG.debug(url + " null Apps again, used " + url + " instead - Attempt-" + (i+1));
            } else if (newSite.getApps()!=null && newSite.getApps().size()==0){
                //System.out.println(" empty Apps again, used " + url + " instead - Attempt-" + (i+1));
                LOG.debug(" empty Apps again, used " + url + " instead - Attempt-" + (i+1));
            }
            i++;
        }

        //System.out.println("got: " + site.getFileLineNo() + " : " + jsonInString);
        LOG.debug("got: " + fileLineNo + " : " + jsonInString);
        return newSite;
    }


    public boolean lookupStatus(Site newSite){
        if (newSite.getApps().size()==0 || (newSite.getApps()!=null && newSite.getApps().size()==0)) {
            //System.out.println("We are not lucky.  No apps for " + newSite.getUrl());
            LOG.debug("We are not lucky.  No apps for " + newSite.getUrl());
        }

        if (newSite.getApps()!=null&&newSite.getApps().size()>0) {
            //System.out.println("Has " + newSite.getApps().size() + " apps");
            LOG.debug("Has " + newSite.getApps().size() + " apps");
            return true;
        }

        return false;
    }

    public void setRunShell(RunShell runShellCmd){
        this.runShellCmd = runShellCmd;
    }
}
