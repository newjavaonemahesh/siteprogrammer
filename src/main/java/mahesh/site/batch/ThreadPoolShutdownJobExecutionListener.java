/*
 *
 *  * Copyright [2017] [Mahesh Chandran K R]
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package mahesh.site.batch;

import org.apache.log4j.Logger;
import org.springframework.batch.core.JobExecution;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

public class ThreadPoolShutdownJobExecutionListener implements org.springframework.batch.core.JobExecutionListener {

    private static final Logger LOG = Logger.getLogger(CSVToDbJobConfiguration.class);

    private TaskExecutor taskExecutor;

    public ThreadPoolShutdownJobExecutionListener(TaskExecutor taskExecutor){
        this.taskExecutor = taskExecutor;
    }


    @Override
    public void beforeJob(JobExecution jobExecution) {

    }

    @Override
    public void afterJob(JobExecution jobExecution) {
        LOG.debug("firing afterJob - shutting down ThreadPoolTaskExecutor");
        ((ThreadPoolTaskExecutor)taskExecutor).shutdown();
    }
}
