package mahesh.site.batch;

import org.apache.log4j.Logger;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

public class CSVToDbJobExecutionListener implements org.springframework.batch.core.JobExecutionListener {

    private static final Logger LOG = Logger.getLogger(CSVToDbJobConfiguration.class);

    private TaskExecutor taskExecutor;

    public CSVToDbJobExecutionListener(TaskExecutor taskExecutor){
        this.taskExecutor = taskExecutor;
    }

    @Override
    public void beforeJob(JobExecution jobExecution) {

    }

    @Override
    public void afterJob(JobExecution jobExecution) {
        LOG.debug("firing afterJob - shutting down ThreadPoolTaskExecutor");
        ((ThreadPoolTaskExecutor)taskExecutor).shutdown();
    }

}