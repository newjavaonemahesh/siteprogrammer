/*
 *
 *  * Copyright [2017] [Mahesh Chandran K R]
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package mahesh.site.batch;

import mahesh.site.model.App;
import mahesh.site.model.Site;
import org.apache.log4j.Logger;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class WappalyzerWriter implements ItemWriter<Site> {

    private static final Logger LOG = Logger.getLogger(WappalyzerWriter.class);

    private static final String GET_APP_BY_NAME = "select app_id, app_name, first_insert from apps where app_name = ?";
    private static final String UPDATE_APP_BY_NAME = "update apps set app_icon=?, website=?, first_insert=? where app_name = ?";
    private static final String INSERT_SITE = "insert into sites (file_line_no, site_url, site_origi_url) VALUES (?,?,?)";
    private static final String INSERT_SITE_APPS = "insert into site_apps (app_id, site_id, confidence, version) VALUES (?,?,?,?)";


    public DataSource dataSource;
    public JdbcTemplate jdbcTemplate;

    @Override
    public void write(List<? extends Site> sites) throws Exception {


        try {

            for (Site site : sites) {

                LOG.info("inside for loop");

                // get the apps list
                List<App> apps = site.getApps();
                List<App> appsForUpdate = new ArrayList<App>();


                for (final App app : apps) {
                    // get the app from apps table
                    App newApp = getAppByName(app);
                    appsForUpdate.add(newApp);
                }

                // now we have a new list, so let's go about updating them

                for (App app : appsForUpdate) {

                    // ensure this record has not been updated yet.
                    if (app.getFirstInsert() == 1) {
                        updateAppByName(app);
                    }
                }

                // insert into site
                Long id = insertSite(site);
                site.setId(id);

                insertSiteApps(site, appsForUpdate);

            }

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }


    public App getAppByName(App app) {
        LOG.debug("query for app " + app.getAppName());
        List matches = jdbcTemplate.query(GET_APP_BY_NAME,
                new Object[]{app.getAppName()},
                new RowMapper<App>() {
                    @Override
                    public App mapRow(ResultSet resultSet, int rowNum)
                            throws SQLException, DataAccessException {

                        App newApp = new App();
                        BeanUtils.copyProperties(app, newApp);
                        // just fill the id
                        newApp.setId(resultSet.getLong(1));
                        newApp.setFirstInsert(resultSet.getInt(3));
                        return newApp;

                    }
                });

        if (matches.size() > 0) {
            return ((App) matches.get(0));
        } else {
            return app;
        }
    }

    public void updateAppByName(App app) {

        LOG.debug("updating app " + app.getAppName() + " with values " +
                app.getAppIcon() + " " + app.getWebsite() + " set " +
                "first insert false " + app.getAppName());

        // not updated, yet, so update with values and set first_insert false.
        jdbcTemplate.update(UPDATE_APP_BY_NAME,
                new Object[]{app.getAppIcon(), app.getWebsite(), 0, app.getAppName()},
                new int[]{Types.VARCHAR, Types.VARCHAR, Types.BOOLEAN, Types.VARCHAR});
    }

    public Long insertSite(Site site) {
        // insert into site

        KeyHolder keyHolder = new GeneratedKeyHolder();

/*
        jdbcTemplate.update(INSERT_SITE,
                new Object[]{site.getFileLineNo(), site.getUrl(), site.getOriginalUrl()},
                new int[]{Types.BIGINT, Types.VARCHAR, Types.VARCHAR});
*/

        jdbcTemplate.update(
                new PreparedStatementCreator() {
                    @Override
                    public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                        PreparedStatement ps =
                                connection.prepareStatement(INSERT_SITE, new String[] {"file_line_no", "site_url", "site_origi_url"});

                        ps.setLong(1, site.getFileLineNo());
                        ps.setString(2, site.getUrl());
                        ps.setString(3, site.getOriginalUrl());
                        return ps;
                    }
                },
                keyHolder
        );


        LOG.debug("insert site with values " + site.getFileLineNo() +
                site.getUrl() + " " + site.getOriginalUrl());


        return keyHolder.getKey().longValue();

    }

    public void insertSiteApps(Site site, List<App> apps) {
        LOG.debug("start inserting site_apps records");
        if (apps==null || apps.size()==0) {
            LOG.debug("No apps inserted for site: " + site + " Reason: No apps found");
            return;
        }

        for (App app: apps) {
            insertSiteApps(site, app);
        }
    }

    public void insertSiteApps(Site site, App app) {
        jdbcTemplate.update(INSERT_SITE_APPS,
                new Object[]{app.getId(), site.getId(), app.getConfidence(), app.getAppVersion()},
                new int[]{Types.BIGINT, Types.BIGINT, Types.VARCHAR, Types.VARCHAR});


        LOG.debug("insert site app record with values " + app.getId() +
                " " + site.getId() + " " + app.getConfidence() +
                " " + app.getAppVersion());

    }

    @Autowired
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }
}
