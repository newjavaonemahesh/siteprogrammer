/*
 * Copyright [2017] [Mahesh Chandran K R]
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package mahesh.site.batch;

import mahesh.site.model.App;
import mahesh.site.model.Site;
import org.apache.log4j.Logger;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.partition.PartitionHandler;
import org.springframework.batch.core.partition.support.MultiResourcePartitioner;
import org.springframework.batch.core.partition.support.Partitioner;
import org.springframework.batch.core.partition.support.TaskExecutorPartitionHandler;
import org.springframework.batch.core.step.tasklet.SystemCommandTasklet;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemStream;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.LineMapper;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.ResourceArrayPropertyEditor;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.core.task.TaskExecutor;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

@Configuration
@Import(JBossInfrastructureConfiguration.class)
public class CSVToDbJobConfiguration {

    private static final Logger LOG = Logger.getLogger(CSVToDbJobConfiguration.class);

    private static final String OVERRIDDEN_BY_EXPRESSION = null;


    @Autowired
    private InfrastructureConfiguration infrastructureConfiguration;




    @Bean
    @StepScope
    public Tasklet fileSplittingTasklet(@Value("#{jobParameters['pathToFile']}")String inputFile,
                                        @Value("#{jobParameters['outDirectory']}") String outDirectory) throws Exception {

        SystemCommandTasklet tasklet = new SystemCommandTasklet();

        tasklet.setCommand(String.format("split -a 3 -l 100 %s %s", inputFile, outDirectory));
        tasklet.setTimeout(60000l);
        tasklet.setWorkingDirectory("/tmp/logs_temp");
        tasklet.afterPropertiesSet();

        return tasklet;
    }

    @Bean(name="lineMapper")
    public LineMapper<Site> lineMapper(){
        DefaultLineMapper<Site> lineMapper = new DefaultLineMapper<Site>();
        DelimitedLineTokenizer lineTokenizer = new DelimitedLineTokenizer();
        lineTokenizer.setNames(new String[]{"lineNo","originalUrl"});
        lineTokenizer.setIncludedFields(new int[]{0,1});
        BeanWrapperFieldSetMapper<Site> fieldSetMapper = new BeanWrapperFieldSetMapper<Site>();
        fieldSetMapper.setTargetType(Site.class);
        lineMapper.setLineTokenizer(lineTokenizer);
        lineMapper.setFieldSetMapper(fieldSetMapper);
        return lineMapper;
    }

/*    @Bean(name="multiReader")
    @StepScope
    public MultiResourceItemReader<Site> itemReader(@Value("#{jobParameters[outDirectory]}") String outDirectory,
                                                    @Qualifier("lineMapper") LineMapper<Site> lineMapper)
                    throws Exception{

        Resource[] resources = getResources(outDirectory);

        FlatFileItemReader<Site> delegate = new FlatFileItemReader<Site>();
        delegate.setLineMapper(lineMapper);
        delegate.afterPropertiesSet();

        MultiResourceItemReader<Site> reader = new MultiResourceItemReader<Site>();
        reader.setDelegate(delegate);
        reader.setResources(resources);

        return reader;

    }*/


    @Bean(name="multiReader")
    @StepScope
    public FlatFileItemReader<Site> itemReader(@Value("#{stepExecutionContext['fileName']}") Resource file,
                                               @Qualifier("lineMapper") LineMapper lineMapper) throws Exception {


        FlatFileItemReader<Site> delegate = new FlatFileItemReader<Site>();
        delegate.setLineMapper(lineMapper);
        delegate.setResource(file);
        delegate.afterPropertiesSet();


        return delegate;
    }

    @Bean(name="processor")
    public DockerRestProcessor processor() {
        DockerRestProcessor processor = new DockerRestProcessor();
        processor.setRunShell(infrastructureConfiguration.runShell());
        return new DockerRestProcessor();
    }

/*
    @Bean(name="writer")
    public ItemWriter<Site> writer(){
        JdbcBatchItemWriter<Site> itemWriter = new JdbcBatchItemWriter<Site>();
        itemWriter.setSql("INSERT INTO sites (FILE_LINE_NO, SITE_ORIGI_URL) VALUES (:fileLineNo,:originalUrl)");
        itemWriter.setDataSource(infrastructureConfiguration.dataSource());
        itemWriter.setItemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<Site>());
        return itemWriter;
    }
*/

    @Bean(name="writer")
    public ItemWriter<Site> writer(){
        return new WappalyzerWriter();
    }

    // Partitioning Beans
    //
    @Bean
    @StepScope
    public PartitionHandler partitionHandler(Step step2) throws Exception {
        TaskExecutorPartitionHandler partitionHandler = new TaskExecutorPartitionHandler();

        partitionHandler.setGridSize(10);
        partitionHandler.setTaskExecutor(new SimpleAsyncTaskExecutor());
        partitionHandler.setStep(step2);

        return partitionHandler;
    }

    @Bean
    @StepScope
    public MultiResourcePartitioner partitioner(@Value("#{jobParameters['outDirectory']}") String outDirectory) {
        MultiResourcePartitioner partitioner = new MultiResourcePartitioner();

        partitioner.setResources(getResources(outDirectory));

        return partitioner;
    }

    @Bean(name="step1")
    public Step step1(StepBuilderFactory stepBuilderFactory,
                      @Qualifier("fileSplittingTasklet") Tasklet fileSplittingTasklet) {
        return stepBuilderFactory.get("step1")
                .tasklet(fileSplittingTasklet)
                .build();
    }

    @Bean(name="step2")
    public Step step2(StepBuilderFactory stepBuilderFactory, @Qualifier("multiReader") ItemStream reader,
                      @Qualifier("processor") DockerRestProcessor processor,
                      @Qualifier("writer") ItemWriter<Site> writer) {
        return stepBuilderFactory.get("step2")
                .<Site, Site>chunk(50)
                .reader((ItemReader<Site>)reader)
                .processor(processor)
                .writer(writer)
                .build();
    }

    @Bean(name="partitionedStep2")
    public Step partitionedStep2(StepBuilderFactory stepBuilderFactory,
                                 PartitionHandler partitionHandler,
            Partitioner partitioner) {
        return stepBuilderFactory.get("partitionedStep2")
                .partitioner("step2", partitioner)
                .partitionHandler(partitionHandler)
                .taskExecutor(taskExecutor())
                .build();
    }




    @Bean
    public Job csvToDbJob(JobBuilderFactory jobBuilderFactory,
                          @Qualifier("step1") Step step1,
                          @Qualifier("partitionedStep2") Step partitionedStep2) {
        return jobBuilderFactory.get("csvToDbJob")
                .incrementer(new RunIdIncrementer())
                .flow(step1)
                .next(partitionedStep2)
                .end()
                .listener(listener(taskExecutor()))
                .build();
    }

    @Bean
    public CSVToDbJobExecutionListener listener(TaskExecutor taskExecutor){
        return new CSVToDbJobExecutionListener(taskExecutor);
    }

    private Resource[] getResources(String outDirectory) {
        ResourceArrayPropertyEditor resourceLoader = new ResourceArrayPropertyEditor();
        resourceLoader.setAsText("file:" + outDirectory + "/*");
        Resource[] resources = (Resource[]) resourceLoader.getValue();
        return resources;
    }

    @Bean
    public TaskExecutor taskExecutor() {
        ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
        taskExecutor.setCorePoolSize(10);
        taskExecutor.setMaxPoolSize(1000);
        taskExecutor.setAllowCoreThreadTimeOut(true);
        taskExecutor.afterPropertiesSet();
        return taskExecutor;
    }

}
