/*
 *
 *  * Copyright [2017] [Mahesh Chandran K R]
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package mahesh.site.batch;

import mahesh.site.model.Site;
import org.apache.log4j.Logger;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;
import java.sql.Types;
import java.util.List;

public class QuickWappalyzerWriter implements ItemWriter<Site> {

    private static final Logger LOG = Logger.getLogger(QuickWappalyzerWriter.class);

    private static final String QUICK_UPDATE_SITE = "update sites set wappalyzer_json=?, quick_insert=? where site_id=?";

    public DataSource dataSource;
    public JdbcTemplate jdbcTemplate;

    @Override
    public void write(List<? extends Site> sites) throws Exception {

        try {
            for (Site site : sites) {
                quickUpdateSite(site);
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    public void quickUpdateSite(Site site) {
        LOG.debug("update site " + site.getId());
        jdbcTemplate.update(QUICK_UPDATE_SITE,
                new Object[]{site.getWappalyzerJson(), 1, site.getId()},
                new int[]{Types.VARCHAR, Types.BOOLEAN, Types.BIGINT});
    }

    @Autowired
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }
}
