/*
 *
 *  * Copyright [2017] [Mahesh Chandran K R]
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package mahesh.site.batch;


import mahesh.site.model.Site;
import org.apache.log4j.Logger;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.partition.PartitionHandler;
import org.springframework.batch.core.partition.support.Partitioner;
import org.springframework.batch.core.partition.support.TaskExecutorPartitionHandler;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemStream;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.database.JdbcPagingItemReader;
import org.springframework.batch.item.database.Order;
import org.springframework.batch.item.database.support.MySqlPagingQueryProvider;
import org.springframework.batch.item.database.support.SqlPagingQueryProviderFactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.core.task.TaskExecutor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

@Configuration
@Import(JBossInfrastructureConfiguration.class)
public class JsonOnlyExtractJobConfiguration {
    private static final Logger LOG = Logger.getLogger(JsonOnlyExtractJobConfiguration.class);

    @Autowired
    private InfrastructureConfiguration infrastructureConfiguration;

    @Bean(name="siteRowMapper")
    public RowMapper<Site> siteRowMapper(){
        return new RowMapper<Site>() {
            @Override
            public Site mapRow(ResultSet resultSet, int i) throws SQLException {
                Site site = new Site();
                site.setId(resultSet.getLong("site_id"));
                site.setFileLineNo(resultSet.getLong("file_line_no"));
                site.setUrl(resultSet.getString("site_url"));

                return site;
            }
        };
    }



    @Bean(name="sitePagingItemReader")
    @StepScope
    public JdbcPagingItemReader<Site> sitePagingItemReader(
                @Value("#{stepExecutionContext['fromNo']}") Long fromNo,
                @Value("#{stepExecutionContext['toNo']}")Long toNo) throws Exception{
        JdbcPagingItemReader<Site> reader = new JdbcPagingItemReader<Site>();
        reader.setDataSource(infrastructureConfiguration.dataSource());
        MySqlPagingQueryProvider queryProvider = new MySqlPagingQueryProvider();;

        queryProvider.setSelectClause("select site_id, file_line_no, site_url");
        queryProvider.setFromClause("from sites");
        queryProvider.setWhereClause("where site_id not in (select distinct site_id from site_apps)" +
                " and site_id >= :fromNo" +
                " and site_id <= :toNo" +
                " and quick_insert=:ins");

        Map<String, Order> sortKeys = new HashMap<>(1);
        sortKeys.put("site_id", Order.ASCENDING);
        queryProvider.setSortKeys(sortKeys);

        Map<String, Object> parameterValues = new HashMap<String, Object>();
        parameterValues.put("fromNo", fromNo);
        parameterValues.put("toNo", toNo);
        parameterValues.put("ins", 0);

        reader.setParameterValues(parameterValues);
        //reader.setPageSize(100);

        reader.setRowMapper(siteRowMapper());

        reader.setQueryProvider(queryProvider);
        reader.afterPropertiesSet();

        return reader;

    }

    @Bean(name="processor1")
    public DockerRestProcessor1 processor1() {
        DockerRestProcessor1 processor = new DockerRestProcessor1();
        processor.setRunShell(infrastructureConfiguration.runShell());
        return processor;
    }

/*    @Bean(name="writer1")
    public LogItemWriter logItemWriter(){
        return new LogItemWriter();
    }*/

    @Bean(name="quickWriter")
    public QuickWappalyzerWriter quickWriter(){
        return new QuickWappalyzerWriter();
    }


    @Bean(name="partitionHandler1")
    @StepScope
    public PartitionHandler partitionHandler1(Step step11) throws Exception {
        TaskExecutorPartitionHandler partitionHandler = new TaskExecutorPartitionHandler();

        partitionHandler.setGridSize(10);
        partitionHandler.setTaskExecutor(new SimpleAsyncTaskExecutor());
        partitionHandler.setStep(step11);

        return partitionHandler;
    }

    @Bean(name="rangePartitioner")
    @StepScope
    public RangePartitioner rangePartitioner() {
        return new RangePartitioner();
    }

    @Bean(name="step11")
    public Step step11(StepBuilderFactory stepBuilderFactory, @Qualifier("sitePagingItemReader") ItemStream reader,
                       @Qualifier("processor1") DockerRestProcessor1 processor1,
                       @Qualifier("quickWriter") QuickWappalyzerWriter quickWriter) {
        return stepBuilderFactory.get("step11")
                .<Site, Site>chunk(1)
                .reader((ItemReader<Site>)reader)
                .processor(processor1)
                .writer(quickWriter)
                .build();
    }

    @Bean(name="partitionedStep11")
    public Step partitionedStep11(StepBuilderFactory stepBuilderFactory,
                                 PartitionHandler partitionHandler1,
                                 Partitioner rangePartitioner) {
        return stepBuilderFactory.get("partitionedStep11")
                .partitioner("step11", rangePartitioner)
                .partitionHandler(partitionHandler1)
                .taskExecutor(taskExecutor1())
                .build();
    }

    @Bean
    public Job jsonOnlyExtractJob(JobBuilderFactory jobBuilderFactory,
                          @Qualifier("partitionedStep11") Step partitionedStep11) {
        return jobBuilderFactory.get("jsonOnlyExtractJob")
                .incrementer(new RunIdIncrementer())
                .flow(partitionedStep11)
                .end()
                .listener(listener1(taskExecutor1()))
                .build();
    }

    @Bean
    public ThreadPoolShutdownJobExecutionListener listener1(TaskExecutor taskExecutor){
        return new ThreadPoolShutdownJobExecutionListener(taskExecutor);
    }

    @Bean
    public TaskExecutor taskExecutor1() {
        ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
        taskExecutor.setCorePoolSize(10);
        taskExecutor.setMaxPoolSize(1000);
        taskExecutor.setAllowCoreThreadTimeOut(true);
        taskExecutor.afterPropertiesSet();
        return taskExecutor;
    }

}
