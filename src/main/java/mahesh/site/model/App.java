/*
 * Copyright [2017] [Mahesh Chandran K R]
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package mahesh.site.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class App {



    private Long id;

    @JsonProperty("name")
    private String appName;

    @JsonProperty("icon")
    private String appIcon;

    @JsonProperty("website")
    private String website;

    @JsonProperty("confidence")
    private int confidence;

    @JsonProperty("version")
    private String appVersion;

    @JsonProperty("categories")
    private List<String> categories;

    @JsonIgnore
    private Integer firstInsert;


    /**
     * No-arg constructor for JavaBean tools
     */
    public App() {}

    // ******************** Accessor Methods ******************** //

    public Long getId(){
        return id;
    }

    public void setId(Long id){
       this.id = id;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getAppIcon() {
        return appIcon;
    }

    public void setAppIcon(String appIcon) {
        this.appIcon = appIcon;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public int getConfidence() {
        return confidence;
    }

    public void setConfidence(int confidence) {
        this.confidence = confidence;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public List<String> getCategories() {
        return categories;
    }

    public void setCategories(List<String> categories) {
        this.categories = categories;
    }

    public void setFirstInsert(Integer firstInsert){
        this.firstInsert = firstInsert;
    }

    public Integer getFirstInsert() {
        return firstInsert;
    }

    // ********************** Common Methods ********************** //

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof App)) return false;

        final App app = (App) o;

        if (appName != null ? !appName.equals(app.appName) : app.appName != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        result = (appName != null ? appName.hashCode() : 0);
        return result;
    }

}
