/*
 *
 *  * Copyright [2017] [Mahesh Chandran K R]
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package mahesh.site.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.log4j.Logger;

public class Category {
    private static final Logger LOG = Logger.getLogger(Category.class);

    private Long id;

    @JsonProperty("name")
    private String categoryName;

    @JsonProperty("priority")
    private String priority;

    /**
     * No-arg constructor for JavaBean tools
     */
    public Category() {}

    // ******************** Accessor Methods ******************** //

    public Long getId(){
        return id;
    }

    public void setId(Long id){
        this.id = id;
    }

    public String getCategoryName() { return categoryName; }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

}
