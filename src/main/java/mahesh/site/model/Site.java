/*
 * Copyright [2017] [Mahesh Chandran K R]
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package mahesh.site.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.log4j.Logger;

import java.util.List;

/**
 * The Site table in our analysis.  It can be one or later contain other dependent tables.
 *
 * @author Mahesh Chandran K R
 */

public class Site {

    private static final Logger LOG = Logger.getLogger(Site.class);

    private Long id;

    private Long fileLineNo;

    @JsonProperty("url")
    private String url;

    @JsonProperty("originalUrl")
    private String originalUrl;

    @JsonProperty("applications")
    private List<App> apps;

    @JsonIgnore
    private String wappalyzerJson;

    /**
     * No-arg constructor for JavaBean tools
     */
    public Site() {}

    public Site(String url) {
        this.url = url;
    }

    public Site(String url, String originalUrl) {
        this(url);
        this.originalUrl = originalUrl;
    }


    // ******************** Accessor Methods ******************** //

    public Long getId(){
        return id;
    }

    public void setId(Long id){
        this.id = id;
    }

    public Long getFileLineNo(){
        return fileLineNo;
    }

    public void setFileLineNo(Long fileLineNo){
        this.fileLineNo = fileLineNo;
    }

    public String getUrl(){
        return url;
    }

    public void setUrl(String url){
        this.url = url;
    }

    public String getOriginalUrl(){
        return originalUrl;
    }

    public void setOriginalUrl(String originalUrl){
        this.originalUrl = originalUrl;
    }

    public List<App> getApps() {
        return apps;
    }

    public void setApps(List<App> apps) {
        this.apps = apps;
    }

    public String getWappalyzerJson(){
        return wappalyzerJson;
    }

    public void setWappalyzerJson(String wappalyzerJson){
        this.wappalyzerJson = wappalyzerJson;
    }

    // ********************** Common Methods ********************** //

    public String toString() {
        return "Site Id ('" + getId() + "'), " +
                "Url: '" + getUrl() + "', " +
                "Original Url: '" + getOriginalUrl() + "'";
    }
}
